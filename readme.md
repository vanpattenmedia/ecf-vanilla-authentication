# Vanilla Authentication

PHP library for remotely authenticating with Vanilla Forums.

## Example

```
<?php

$auth           = new VanillaAuthentication( 'http://my-forum-name.vanillaforums.com' );
$auth->email    = 'user@domain.com';
$auth->password = 'abcd1234';

if ( $user = $auth->authenticate() ) {
	echo "Welcome, {$user['Profile']['Name']}";
} else {
	echo 'Your credentials were invalid.';
}
```
